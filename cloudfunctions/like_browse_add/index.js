// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const _ = db.command


// 云函数入口函数
exports.main = async (event, context) => {
  await db.collection('activity').doc(event.id).update({
    data: {
      // 表示指示数据库将字段自增 喜欢占比70% 浏览占比30%
      like: _.inc(event.like),
      browse: _.inc(event.browse),
      heat: _.inc(event.like*0.7 + event.browse*0.3)
    }  
  })
  if(event.like>0)
  {
    // 更新添加新的点赞记录
    await db.collection('user_info').where({
      _openid: event.userInfo.openId
    }).update({
      data: {
        'like': _.unshift(event.id)
      }  
    })
    return {_zan: true}
  }
  else{
    await db.collection('user_info').where({
      _openid: event.userInfo.openId
    }).get().then(async res=>{
      for(let i =0; i<res.data[0].like.length; i++)
      {
        if(res.data[0].like[i] == event.id){
          // 获取删除后的数组
          res.data[0].like.splice(i, 1)
          await db.collection('user_info').where({
            _openid: event.userInfo.openId
          }).update({
            data: {
              'like': res.data[0].like
            }  
          })
          break
        }
      }
    })
    return {_zan: false}
  }

}