// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  let like = []
  let name = '游客'
  await db.collection('user_info').where({
    _openid: event.userInfo.openId
  }).get().then(async res=>{
    // 如果用户不存在,创建数据
    if(res.data.length == 0){
      await db.collection('user_info').add({
        data: {
          _openid: event.userInfo.openId,
          browse: [],
          like: [],
          user_head: '',
          user_name: '游客'
        }
      })
    }
    else{
      like = res.data[0].like
      name = res.data[0].user_name
    }
  })
  return {like:like, user_name: name, openId: event.userInfo.openId}

}