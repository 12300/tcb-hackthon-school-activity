# tcb-hackthon-school-activity

 **#### 介绍**  
有活有色，依托于腾讯云开发，解决了校园生活中无法获得想要的活动的问题，通过云开发获取管理员发布或者后端爬虫写入数据库的活动，并通过浏览与点 
 赞量来推荐热门活动，提供了活动类型，结束开始时间，以及活动的搜索等功能。  

 **#### 特点**   
有活有色，活指示了活动，色代表了本小程序采用了大量的色彩，以暖色调勾起用户对活动的热情，目前卡片有5种颜色随机，活动详细页也由5种颜色随机。        


 **#### 安装教程**     

1.  下载开发者工具  
2.  使用云开发，并导入代码  
3.  上传并部署云函数  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0406/115601_fb84e211_606443.png "屏幕截图.png")  
将cloudfunctions中的文件全部上传并部署
4.  在云开发数据库中，创建集合  
| 集合名    | 集合权限       | 是否必须 |  
| --------- | -------------- | -------- |  
| activity  | 默认，第一项   | 是       |  
| user_info | 仅创建者可读写 | 是       |  


在云开发数据库中创建创建user_info，activity两个集合  
ps：如果您有多个云开发环境请在app.js中修改 env: '您的环境名称',      


 **### 项目结构**   
├─colorui  
│  └─components  
├─components  
│  ├─head  
│  └─returnTop  
├─images  
├─pages  
│  ├─article  
│  ├─browse  
│  ├─home  
│  ├─index  
│  ├─lately  
│  ├─like  
│  ├─minute  
│  ├─new  
│  └─seach  
└─style    

 **#### 参与贡献**     

1.  Fork 本仓库  
2.  新建 Feat_xxx 分支  
3.  提交代码  
4.  新建 Pull Request       


 **#### 项目截图**   
1.index页面，索引页  
![index](https://images.gitee.com/uploads/images/2020/0406/112103_5a3fa1e9_606443.png "Snipaste_2020-04-06_11-20-52.png")  
2.home页面，个人中心（可在云数据库中user_info修改用户类型为type，即可发布文章  
![home](https://images.gitee.com/uploads/images/2020/0406/112152_80f9f552_606443.png "屏幕截图.png")
![home](https://images.gitee.com/uploads/images/2020/0406/112538_0a7e139b_606443.png "屏幕截图.png")  
3.new页面，发布活动页面  
![new](https://images.gitee.com/uploads/images/2020/0406/112726_65da8d24_606443.png "屏幕截图.png")  
4.minute页面，活动详细页  
![0](https://images.gitee.com/uploads/images/2020/0406/112854_242a6f67_606443.png "屏幕截图.png")
![1](https://images.gitee.com/uploads/images/2020/0406/113420_3e40db06_606443.png "屏幕截图.png")  
目前有2种类型活动，一种是由爬虫直接爬取公众号的活动，一种是由管理员发布的活动，在数据库分别标记type为0、1  
5.seach页面，搜索活动  
![seach](https://images.gitee.com/uploads/images/2020/0406/113127_586b304d_606443.png "屏幕截图.png")      


 **#### 后续开发**  
1.首页标签分类  
2.后台管理员能快速切换为用户  
3.活动订阅功能  
4.活动结束提醒功能  