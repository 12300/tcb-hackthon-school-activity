// pages/templat/templat.js
const db = wx.cloud.database()
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let like = []
    await db.collection('user_info').get().then(res=>{
      like = res.data[0].like
    })
    db.collection('activity').orderBy('start_time', 'desc').limit(10).where(_.or([
      {
        _id: _.in(like)
      }
    ])
    ).get().then(res => {
      let new_random_color = [] // 随机新文章颜色
      // 设置新文章随机
      for (let i = 0; i < res.data.length; i++) {
        new_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
      }
      this.setData({
        new_activity: res.data,
        new_random_color
      })
    })
  },
  // 详情页跳转
  navigator_minute(event) {
    if(new Date().getTime() - this.data.nav_time <1.5){
      console.log('频繁点击')
      return
    }
    this.data.nav_time = new Date().getTime()

    if (event.currentTarget.dataset.type == 0)
      wx.navigateTo({
        url: '/pages/minute/minute?id=' + event.currentTarget.dataset.id + '&openId=' + this.data.openId
      })
    else {
      console.log('公众号文章')
    }
  },
  

 

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: async function () {
    let like = []
    await db.collection('user_info').get().then(res=>{
      like = res.data[0].like
    })
    db.collection('activity').skip(this.data.new_activity.length).orderBy('start_time', 'desc').limit(10).where(_.or([
      {
        _id: _.in(like)
      }
    ])
    ).get().then(res => {
      let new_random_color = [] // 随机新文章颜色
      // 设置新文章随机
      for (let i = 0; i < res.data.length; i++) {
        new_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
      }
      new_random_color = this.data.new_random_color.concat(new_random_color)
      this.setData({
        new_activity: res.data,
        new_random_color
      })
    })
  },

 
})