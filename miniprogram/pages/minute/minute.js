// pages/minute/minute.js
const db = wx.cloud.database()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    show : false,
    click_time: 0,
     // 活动标题
    title: '',
    // 活动类型
    form: '',
    // 活动内容
    matter: '',
    // 位置
    site: '',
    // 开始时间
    start_time: '',
    // 结束时间
    end_time: '',
    dian_: false,
    id: '',
    act: 0,
    min_height: wx.getSystemInfoSync().windowHeight + wx.getSystemInfoSync().statusBarHeight
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    // 判断是否点攒了
    await db.collection('user_info').where({
      _openid: options.openId
    }).get().then(res=>{
      this.setData({
        dian_: res.data[0].like.includes(options.id),
        id: options.id
      })
    })
    await db.collection('activity').doc(options.id).get().then(res=>{
      this.setData({
        title: res.data.title,
        form: res.data.form,
        matter: res.data.matter,
        site: res.data.site,
        start_time: res.data.start_time,
        end_time: res.data.end_time,
        id: options.id,
        show: true
      })
      this.setData({
        act: 1
      })
    })
  },
  click_zan(event){
    if(new Date().getTime() - this.data.click_time <1000){
      wx.showToast({
        title: '请勿频繁点赞哟',
        icon: 'none',
        duration: 1000
      })
      return
    }
    this.data.click_time = new Date().getTime()
    // console.log(this.data)
    // 点击切换赞状态
    let dian_ = this.data.dian_
      this.setData({
        dian_: !dian_
      })
    // 当点了赞
    if(!dian_){
      
      // 更新数据库
      wx.cloud.callFunction({
        name: 'like_browse_add',
        data:{
          id: this.data.id,
          like: 1,
          browse: 0
        }
      })
    }else{ // 取消赞
      wx.cloud.callFunction({
        name: 'like_browse_add',
        data:{
          id: this.data.id,
          like: -1,
          browse: 0
        }
      })
    }
  },
  
})