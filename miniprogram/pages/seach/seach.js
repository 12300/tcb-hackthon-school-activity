// pages/templat/templat.js
const db = wx.cloud.database()
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      value: options.value
    })
    db.collection('activity').orderBy('start_time', 'desc').limit(10).where(_.or([
      {
        title: db.RegExp({
          regexp: options.value
        })
      },
      {
        matter: db.RegExp({
          regexp: options.value
        })
      }
    ])
    ).get().then(res => {
      let new_random_color = [] // 随机新文章颜色
      // 设置新文章随机
      for (let i = 0; i < res.data.length; i++) {
        new_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
      }
      this.setData({
        new_activity: res.data,
        new_random_color
      })
    })
  },
  // 详情页跳转
  navigator_minute(event) {
    if(new Date().getTime() - this.data.nav_time <1.5){
      console.log('频繁点击')
      return
    }
    this.data.nav_time = new Date().getTime()

    if (event.currentTarget.dataset.type == 0)
      wx.navigateTo({
        url: '/pages/minute/minute?id=' + event.currentTarget.dataset.id + '&openId=' + this.data.openId
      })
    else {
      console.log('公众号文章')
    }
  },
  

 

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {      
    db.collection('activity').skip(this.data.new_activity.length).orderBy('start_time', 'desc').limit(10).where(_.or([
    {
      title: db.RegExp({
        regexp: options.value
      })
    },
    {
      matter: db.RegExp({
        regexp: options.value
      })
    }])).get().then(res => {
        let new_activity = this.data.new_activity.concat(res.data)
        let new_random_color = [] // 随机新文章颜色
        // 设置新文章随机颜色
        for (let i = 0; i < res.data.length; i++) {
          new_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
        }
        new_random_color = this.data.new_random_color.concat(new_random_color)
        this.setData({
          new_activity,
          new_random_color
        })
      })
  },
})