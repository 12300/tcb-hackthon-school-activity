// pages/home/home.js
const db = wx.cloud.database()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    user_head: '',
    user_name: '游客',
    // 最近喜欢
    like: 0,
    // 历史浏览
    browse: 0,
    min_height: wx.getSystemInfoSync().windowHeight + wx.getSystemInfoSync().statusBarHeight
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let like = 0;
    let browse = 0;
    let user_head = ''
    let user_name = ''
    // 获取用户信息
    db.collection('user_info').get().then(res=>{
      like = res.data[0].like.length;
      browse = res.data[0].browse.length;
      user_head = res.data[0].user_head
      user_name = res.data[0].user_name
      this.setData({
        like,
        browse,
        user_name,
        user_head
      })
    })
    
  },
  my_like(event){
    wx.navigateTo({
      url: '/pages/like/like'
    })
  },
  my_browse(event){
    wx.navigateTo({
      url: '/pages/browse/browse'
    })
  }
  
})