// miniprogram/pages/index/index.js
const db = wx.cloud.database()
Page({
  data: {
    // 跳转时间
    nav_time: 0,
    // 点击时间
    dian_time: 0,
    // 用户名
    user_name: '',
    // 存储用户赞过的对象
    user_like: [],
    // 文章数据
    heat_activity: [],
    new_activity: [],
    // 点赞数据
    dian_: [],
    // 赞数据
    like: [],
    heat_random_color: [], // 热门文章颜色数据
    new_random_color: [], //新文章颜色数据,
    openId: ''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取热门活动
    db.collection('activity').limit(10).orderBy('heat', 'desc').get().then(res => {
      let dian_ = []
      let like = []
      let heat_random_color = [] //随机热门文章颜色
      wx.cloud.callFunction({
        name: 'get_user_zan',
        success: re => {
          this.setData({
            user_like: re.result.like,
            user_name: re.result.user_name,
            openId: re.result.openId
          })
          // 默认不点赞
          for (let i = 0; i < res.data.length; i++) {
            if (this.data.user_like.includes(res.data[i]._id))
              dian_[i] = true
            else
              dian_[i] = false
          }
          // 设置喜欢数
          for (let i = 0; i < res.data.length; i++) {
            like[i] = res.data[i].like
          }
          // 设置热门文章随机颜色
          for (let i = 0; i < res.data.length; i++) {
            heat_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
          }
          this.setData({
            heat_activity: res.data,
            like,
            dian_,
            heat_random_color
          })
        }
      })
    })
    db.collection('activity').limit(10).get().then(res => {
      let new_random_color = [] // 随机新文章颜色
      // 设置新文章随机
      for (let i = 0; i < res.data.length; i++) {
        new_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
      }
      this.setData({
        new_activity: res.data,
        new_random_color
      })
    })
  },
  bindconfirm(event){
    wx.navigateTo({
      url: '/pages/templat/templat?value=' + event.detail.value,
    })
  },
  bindinput(event){
    console.log(event)
  },
  // 详情页跳转
  navigator_minute(event) {
    if(new Date().getTime() - this.data.nav_time <1.5){
      console.log('频繁点击')
      return
    }
    this.data.nav_time = new Date().getTime()

    if (event.currentTarget.dataset.type == 0)
      wx.navigateTo({
        url: '/pages/minute/minute?id=' + event.currentTarget.dataset.id + '&openId=' + this.data.openId
      })
    else {
      console.log('公众号文章')
    }
  },

  click_zan(event) {
    if(new Date().getTime() - this.data.dian_time <1000){
      wx.showToast({
        title: '请勿频繁点赞哟',
        icon: 'none',
        duration: 1000
      })
      return
    }
    this.data.dian_time = new Date().getTime()
    // 点击切换赞状态
    let dian_ = this.data.dian_
    let index = event.currentTarget.dataset.index
    let like = this.data.like

    // dian_[index] = !dian_[index]
    
    // 当点了赞
    if (!dian_[event.currentTarget.dataset.index]) {
      // 显示赞加一
      like[index]++
      dian_[index] = !dian_[index]
      this.setData({
        like,
        dian_
      })
      // 更新数据库
      wx.cloud.callFunction({
        name: 'like_browse_add',
        data: {
          id: this.data.heat_activity[index]._id,
          like: 1,
          browse: 0
        }
      }).then(res=>{
        
      })
     
    } else { // 取消赞
        // 显示赞减一
        dian_[index] = !dian_[index]    
        like[index]--
        this.setData({
          like,
          dian_
        })
        wx.cloud.callFunction({
        name: 'like_browse_add',
        data: {
          id: this.data.heat_activity[index]._id,
          like: -1,
          browse: 0
        }
      }).then(res=>{})
    }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    db.collection('activity')
      .skip(this.data.new_activity.length)
      .limit(10)
      .get()
      .then(res => {
        let new_activity = this.data.new_activity.concat(res.data)
        let new_random_color = [] // 随机新文章颜色
        // 设置新文章随机颜色
        for (let i = 0; i < res.data.length; i++) {
          new_random_color[i] = (Math.floor(Math.random() * 10)) % 5 + 1;
        }
        new_random_color = this.data.new_random_color.concat(new_random_color)
        this.setData({
          new_activity,
          new_random_color
        })
      })
  },
  async bindgetuserinfo(event){
    // console.log(event)
    if(event.detail.errMsg == 'getUserInfo:ok'){
      wx.cloud.callFunction({
        name: 'set_user_info',
        data: event.detail.userInfo
      })
      wx.navigateTo({
        url: '/pages/home/home',
      })
    }
  }

})